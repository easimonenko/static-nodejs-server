# README #

static-nodejs-server -- статический веб-сервер на базе Node.js

static-nodejs-server -- свободно-распространяемое программное обеспечение,
распространяется под лицензией MIT.

### Документация ###

Для запуска сервера:

```
cd static-nodejs-server\bin
development (или production)
```

### Разработчик проекта ###

Симоненко Евгений А. (easimonenko) <easimonenko@mail.ru>.

В проекте используются наработки проекта 
https://bitbucket.org/severeisland/testero
одним из разработчиков которого являет автор.
